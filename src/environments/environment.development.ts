export const environment = {
    production: true,
    apiUrl: 'http://localhost:3000',
    loginUrl: '/api-local/auth/login',
    productsUrl: '/api-local/Products'
};
