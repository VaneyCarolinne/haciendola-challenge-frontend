import { ProductFiltersKeysMap } from "./types";

export const QUERY_SORT_NAME = 'orden';

export const QUERY_SORT_NAME_DIR = 'orden-dir';

export enum filterSortOrder {
    desc = 'desc',
    asc = 'asc',
}

export const QUERY_FILTERS_ALIASES: ProductFiltersKeysMap = {
    page: 'pagina',
    limit: 'limite',
    ad: 'ad',
    type: 'tipo',
    startDate: 'inicio',
    endDate: 'termino',
    sort: 'orden',
    sortDirection: 'orden-dir',
}

export const PRODUCT_PAGE_SIZE_OPTIONS = [20, 50, 100];

export const DEFAULT_PRODUCTS_PAGE_SIZE = PRODUCT_PAGE_SIZE_OPTIONS[2];

export const ROUTE_PRODUCT_PARAM = 'id';