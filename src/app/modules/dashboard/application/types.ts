export interface LoginPayload {
    username: string;
    password: string;
}

export interface JWToken {
    access_token: string;
}

export class Product {
  constructor(id: number, handle: string,  title: string,
     description: string,  sku: string,
     grams: string,  stock: number,
     price: number,  comparePrice: number,
     barcode: string
  ) {
  }
}

export interface Product {
    id: number;
    handle: string;
    title: string;
    description: string;
    sku: string;
    grams: string;
    stock: number;
    price: number;
    comparePrice: number;
    barcode: string
}

export interface ProductResponse {
    products: Product[];
}

export type SortDirection = 'asc' | 'desc';

export type ProductFiltersKeysMap = {
  page: string;
  limit: string;
  ad: string;
  type: string;
  startDate: string;
  endDate: string;
  sort: string;
  sortDirection: string;
};

export interface IAnyObject {
  /** any prop with any value */
  /* eslint-disable @typescript-eslint/no-explicit-any */
  [property: string]: any;
}

export type ProductsFilters = {
  page: number;
  limit: number;
  startDate?: string | null;
  endDate?: string | null;
  sort?: string;
  sortDirection?: string;
}

export type ProductsFiltersKeysMap = {
  page: string;
  limit: string;
  startDate: string;
  endDate: string;
  sort: string;
  sortDirection: string;
};

export const QUERY_FILTERS_BACKEND: ProductsFiltersKeysMapBackend = {
    page: { label: 'page' },
    limit: { label: 'limit' },
    startDate: { label: 'start_date', method: 'date' },
    endDate: { label: 'end_date', method: 'date' },
    sort: { label: 'sort' },
    sortDirection: { label: 'sort-dir' },
}

export type ProductsFiltersKeysMapBackend = {
  page: ProductsFiltersKeysMapBackendConfig;
  limit: ProductsFiltersKeysMapBackendConfig;
  startDate: ProductsFiltersKeysMapBackendConfig;
  endDate: ProductsFiltersKeysMapBackendConfig;
  sort: ProductsFiltersKeysMapBackendConfig;
  sortDirection: ProductsFiltersKeysMapBackendConfig;
};

export type ProductsFiltersKeysMapBackendConfig = {
  label: string;
  method?: ProductsFiltersKeysMapBackendConfigMethod;
};

/** methods to transform data to backend */
export type ProductsFiltersKeysMapBackendConfigMethod = 'raw' | 'date';