import { Injectable } from '@angular/core';
import { DEFAULT_PRODUCTS_PAGE_SIZE } from '../constants';
import { Product, ProductResponse, ProductsFilters, ProductsFiltersKeysMap, ProductsFiltersKeysMapBackendConfigMethod, QUERY_FILTERS_BACKEND } from '../types';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

    constructor(
        private http: HttpClient,
    ) {}
  

    hasFilters(filters: ProductsFilters): boolean {
        return !!(filters.page !== 1 || filters.limit !== DEFAULT_PRODUCTS_PAGE_SIZE  ||
        this.hasAdditionalFilters(filters));
    }

    hasAdditionalFilters(filters: ProductsFilters): boolean {
        return !!(filters.endDate || filters.startDate);
    }

    private getOptions(): { headers: HttpHeaders } {
        return {
            headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                .append('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
                .append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
                .append('Allow', 'GET, POST, OPTIONS, PUT, DELETE'),
        };
    }

    getHeaders(): HttpHeaders {
        const session = localStorage.getItem('accessToken');
        if (session) {
            return new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                .append('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
                .append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
                .append('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
                .set('Authorization', 'Bearer ' + session);
        }
        return new HttpHeaders();
    }
 
    queryProducts(filters: ProductsFilters): Observable<Product[]> {
        // const urlParams = new URLSearchParams();

        // Object.keys(QUERY_FILTERS_BACKEND).forEach(k => {
        //     const value = filters[k as keyof ProductsFilters];
        //     if (value) {
        //         const config = QUERY_FILTERS_BACKEND[k as keyof ProductsFiltersKeysMap];
        //         const parsedValue = this.transformBackendValue(value, config.method);
        //         if (parsedValue) {
        //             urlParams.set(config.label, parsedValue);
        //         }
        //     }
        // });
        
        return this.http.get<Product[]>(`${environment.productsUrl}`, {
            headers: this.getHeaders(),
        });
    }

    transformBackendValue(value: number | string, method: ProductsFiltersKeysMapBackendConfigMethod = 'raw'): string {
        switch (method) {
            case 'raw':
                return value.toString();
            case 'date':
                const date = new Date(Number(value));
                if (!isNaN(date.valueOf())) {
                    return date.toISOString();
                }
        }
        return '';
    }

    getProductById(id: number): Observable<Product> {
        return this.http.get<Product>(`${environment.productsUrl}/${id}`, {
            headers: this.getHeaders(),
        });
    }

}

 
