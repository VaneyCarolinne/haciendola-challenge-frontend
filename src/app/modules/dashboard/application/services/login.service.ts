import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JWToken, LoginPayload } from '../types';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http: HttpClient,
  ) {}
  
  private getOptions(): { headers: HttpHeaders } {
    return {
      headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')
        .append('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method')
        .append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
        .append('Allow', 'GET, POST, OPTIONS, PUT, DELETE'),
    };
  }

    login(data: LoginPayload): Observable<JWToken> {
        return this.http.post<JWToken>(environment.loginUrl, { 
            username: data.username,
            password: data.password
        }, this.getOptions());
    }
 

}
