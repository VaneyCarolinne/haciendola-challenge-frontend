import { ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filterSortOrder, QUERY_SORT_NAME, QUERY_SORT_NAME_DIR } from '../../../application/constants';
import { SortDirection } from '../../../application/types';

/** apply a sort and sort direction to url query params */
@Component({
  selector: 'app-table-header-sort',
  templateUrl: './table-header-sort.component.html',
  styleUrls: ['./table-header-sort.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableHeaderSortComponent implements OnInit, OnDestroy {
  /** configure sort query prop name */
  @Input() sortProp: string = QUERY_SORT_NAME;

  /** configure sort direction query prop name */
  @Input() sortDirectionProp: string = QUERY_SORT_NAME_DIR;

  /** property name for this field as sorting item */
  @Input() prop: string = '';

  /**
   * state of sorting in this component
   * 0: disabled (not sorting this), 1: desc, 2: asc
   * */
  state: 0 | 1 | 2 = 0;

  /** subscription to query params change */
  queryParamsSub?: Subscription;

  /** component constructor */
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { /* do nothing */ }

  /** subscribe to external changes */
  ngOnInit(): void {
    this.queryParamsSub = this.activatedRoute.queryParams.subscribe(this.onQueryParams.bind(this));
  }

  /** clean up */
  ngOnDestroy(): void {
    this.queryParamsSub?.unsubscribe();
  }

  /** apply the sort options when user click this component */
  @HostListener('click') onClick() {
    // TODO: DT-1214: not working in endpoint
    // this.router.navigate([], {
    //   queryParams: this.getNextRoute(),
    //   queryParamsHandling: 'merge',
    // });
  }

  /** the route sort options are looped in below order */
  getNextRoute(): Params {
    switch (this.state) {
      case 0: // -> desc
        return { [this.sortProp]: this.prop, [this.sortDirectionProp]: filterSortOrder.desc };
      case 1: // -> asc
        return { [this.sortProp]: this.prop, [this.sortDirectionProp]: filterSortOrder.asc };
      case 2: // -> turn off
        return { [this.sortProp]: null, [this.sortDirectionProp]: null };
    }
  }

  /** when query params change, update the internal state and view values */
  onQueryParams(): void {
    if (this.getEnable()) {
      this.state = this.getDirection() === filterSortOrder.desc ? 1 : 2;
    } else {
      this.state = 0;
    }
    this.changeDetectorRef.detectChanges();
  }

  /** check if user is sorting by this property (this.prop) */
  getEnable(): boolean {
    return this.activatedRoute.snapshot.queryParams[this.sortProp] &&
      this.activatedRoute.snapshot.queryParams[this.sortProp] === this.prop;
  }

  /** get direction of current sorting options, if there is any */
  getDirection(): SortDirection | null {
    const direction = this.activatedRoute.snapshot.queryParams[this.sortDirectionProp];
    if (direction === filterSortOrder.desc || direction === filterSortOrder.asc) {
      return direction;
    }
    return null;
  }
}
