import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Params } from '@angular/router';
import paginate from 'jw-paginate';
import { IAnyObject } from 'src/app/modules/dashboard/application/types';
import { PRODUCT_PAGE_SIZE_OPTIONS, QUERY_FILTERS_ALIASES } from '../../../application/constants';

/** display pagination links of data table */
@Component({
  selector: 'app-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TablePaginationComponent implements OnChanges {
  /** current page */
  @Input() page: number = 1;

  /** total items of all pages */
  @Input() total: number = 0;

  /** items per page */
  @Input() pageSize: number = 0;

  /** configure page prop name in route query params */
  @Input() pageNameProp: string = QUERY_FILTERS_ALIASES.page;

  /** configure pageSize (items per page) prop name in route query params */
  @Input() pageSizeProp: string = QUERY_FILTERS_ALIASES.limit;

  /** options to user selectionable page sizes */
  pageSizes: number[] = PRODUCT_PAGE_SIZE_OPTIONS;

  /** max amount of links pages */
  maxPages: number = 4;

  /** Array with visible pages */
  pages: number[] = [];

  /** Pagination start visible page */
  startPage: number = 1;

  /** Pagination end visible page */
  endPage: number = 1;

  /** Pagination last posible page */
  totalPages: number = 1;

  /** update pagination view on external changes */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['page']) {
      this.setPagination(this.total, changes['page'].currentValue, this.pageSize, this.maxPages);
    }
    if (changes['total']) {
      this.setPagination(changes['total'].currentValue, this.page, this.pageSize, this.maxPages);
    }
    if (changes['pageSize']) {
      this.setPagination(this.total, this.page, changes['pageSize'].currentValue, this.maxPages);
    }
  }

  /** Update the pagination values for display the UI */
  setPagination(total: number, page: number, pageSize: number, maxPages: number): void {
    const pagination: IAnyObject = paginate(total, page, pageSize, maxPages);
    this.pages = pagination['pages'];
    this.startPage = pagination['startPage'];
    this.endPage = pagination['endPage'];
    this.totalPages = pagination['totalPages'];
  }

  /** generate params object for page link */
  getQueryParams(page: number): Params {
    return { [this.pageNameProp]: page };
  }

  /** generate params object for page size change link, this reset the view to page 1 (default page) */
  pageSizeQueryParams(pageSize: number): Params {
    return { [this.pageSizeProp]: pageSize, [this.pageNameProp]: null };
  }
}
