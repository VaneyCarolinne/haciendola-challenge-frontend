import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Product, ProductsFilters } from "../../../application/types";
import { ProductService } from "../../../application/services/product.service";

@Component({
  selector: "app-product-list",
  templateUrl: "./product-list.component.html",
  styleUrls: ["./product-list.component.scss"],
  
})
export class ProductListComponent implements OnInit{

    constructor( 
        @Inject(DOCUMENT) private document: Document,
        private productService: ProductService,
        private changeDetectorRef: ChangeDetectorRef,
    ) {}

    /** loading state */
    loading: boolean = false;

    count: number = 265;
    page: number = 1;
    limit: number = 20;

    filters: ProductsFilters = { limit: 10, page: 1 };

    hasFilters: boolean = false;

    error: number = 0;

    productResponse?: Product[];

    ngOnInit(): void {
        this.filtersUpdated(this.filters);
    }

    filtersUpdated(filters: ProductsFilters): void {
        this.filters = filters;
        this.hasFilters = this.productService.hasFilters(filters);
        this.changeDetectorRef.detectChanges();
        this.loadProducts();
    }

    async loadProducts(): Promise<void> {
        if (this.filters) {
            this.error = 0;
            this.loading = true;
            const products =  await this.productService.queryProducts(this.filters).toPromise();
            this.productResponse = products;
            this.changeDetectorRef.detectChanges();
        }
    }


}