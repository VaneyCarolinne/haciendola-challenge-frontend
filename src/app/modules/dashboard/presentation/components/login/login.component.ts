import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { LoginService } from "../../../application/services/login.service";
import { JWToken, LoginPayload } from "../../../application/types";
import { CommonModule, DOCUMENT } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  standalone: true,
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  providers: [LoginService],
  imports: [ FormsModule, CommonModule, HttpClientModule],
  
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  accessToken: string = '';

  constructor( private loginService: LoginService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    @Inject(DOCUMENT) private document: Document,
  ) {}

   login(): void {
    this.singIn({ username: this.username, password: this.password });
    this.changeDetectorRef.detectChanges();
    this.router.navigate(['/productDashboard']);
  }

  singIn(user: LoginPayload): void {
    this.loginService.login(user)
      .subscribe(result => {
        this.accessToken = result.access_token
        localStorage.setItem('accessToken', result.access_token);
      } )
      .add(() => this.changeDetectorRef.detectChanges());
  }

}