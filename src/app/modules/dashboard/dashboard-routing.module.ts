import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './presentation/components/login/login.component';
import { ProductListComponent } from './presentation/components/product-list/product-list.component';
import { ProductComponent } from './modules/productDetail/presentation/product/product.component';
import { ROUTE_PRODUCT_PARAM } from './application/constants';


const routes: Routes = [
   { path: '', component: LoginComponent },
   { path: 'productDashboard', component: ProductListComponent},
   {
            path: `productDashboard/:id`,
            loadChildren: () => import('./modules/productDetail/productDetail.module').then(m => m.ProductDetailModule),
  },
  
 
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }