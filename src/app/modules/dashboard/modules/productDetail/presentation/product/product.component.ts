import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ROUTE_PRODUCT_PARAM } from 'src/app/modules/dashboard/application/constants';
import { ProductService } from 'src/app/modules/dashboard/application/services/product.service';
import { Product } from 'src/app/modules/dashboard/application/types';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductComponent implements OnInit, OnDestroy {
  routeParamsSub?: Subscription;
  loading: boolean = false;
  product?: Product;
  error: number = 0;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
  ) {  }


  async ngOnInit(): Promise<void> {
    this.routeParamsSub = this.activatedRoute.params.subscribe(await this.onActivatedRouteParams.bind(this));
  }

  /** clean up */
  ngOnDestroy(): void {
    this.routeParamsSub?.unsubscribe();
  }


  async onActivatedRouteParams(): Promise<void> {
    const productId = Number(this.activatedRoute.snapshot.params[ROUTE_PRODUCT_PARAM]);
    if (productId > 0) {
      this.loading = true;
      const product=  await this.productService.getProductById(productId).toPromise();
      this.product = product
      this.changeDetectorRef.detectChanges();
    }  
  }

  exit(): void {
    this.router.navigate([`/productDashboard`]);
  }
}
