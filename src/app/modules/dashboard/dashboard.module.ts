import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { LoginComponent } from './presentation/components/login/login.component';
import { ProductListComponent } from './presentation/components/product-list/product-list.component';
import { TableHeaderSortComponent } from './presentation/components/table-header-sort/table-header-sort.component';
import { TablePaginationComponent } from './presentation/components/table-pagination/table-pagination.component';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ProductDetailModule } from './modules/productDetail/productDetail.module';

@NgModule({
  declarations: [
    ProductListComponent,
    TableHeaderSortComponent,
    TablePaginationComponent,
    TableHeaderSortComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    LoginComponent,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatMenuModule,
    MatIconModule,
  ],
})
export class DashboardModule { }