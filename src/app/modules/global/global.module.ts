import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app/app.component';
import { DashboardModule } from '../dashboard/dashboard.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    DashboardModule,
    CommonModule,
    RouterModule,
    HttpClientModule,
  ],
  exports: [
   
  ],
  providers: [
   
  ],
})
export class GlobalModule { }